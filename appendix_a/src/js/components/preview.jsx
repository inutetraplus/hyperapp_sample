import { h } from 'hyperapp'

export default ({state}) => (
  <section id='previewHtml'>
    <div
      id='preview'
      className={state.previewStyle}
      innerHTML={state.preview}>
    </div>
  </section>
)