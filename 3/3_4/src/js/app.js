import { h, app } from 'hyperapp'
import marked from 'marked'

import './../scss/style.scss'

const previewStyles = ['github', 'air'] // <- CSSのリストを配列で管理
const state = {
  preview: '',
  previewStyle: previewStyles[0] // <- 追加
}

const actions = {
  setInput: input => state => ({
    preview: marked(input)
  }),
  changeCss: input => state => ({ // <- 追加
    previewStyle: input
  })
}

const view = (state, actions) => (
  <div>
    <header id='header'> {/* <- 追加 */}
      <h1>Hyperapp Markdown Editor</h1>
      <p id='selectWrap'>
        <label>
          CSS type :
          <select id='selectCss' onchange={e => actions.changeCss(e.target.value)}>
            {previewStyles.map(previewStyle => {
              return <option value={previewStyle}>{previewStyle}</option>
            })}
          </select>
        </label>
      </p>
    </header>

    <article id='main'>
      <section id='inputMarkdown'>
        <p id='editorWrap'>
          <textarea
            id='editor'
            placeholder='# input markdown'
            oninput={e => actions.setInput(e.target.value)}
          />
        </p>
      </section>

      <section id='previewHtml'> {/* <- classNameを追加 */}
        <div
          id='preview'
          className={state.previewStyle}
          innerHTML={state.preview}>
        </div>
      </section>
    </article>
  </div>
)

app(state, actions, view, document.body)