import { h, app } from 'hyperapp'
import marked from 'marked'

import './../scss/style.scss'

const editorItems = [
  {
    name: 'strong',
    before: '**',
    after: '**'
  },
  {
    name: 'italic',
    before: '*',
    after: '*'
  },
  {
    name: 'strikethrough',
    before: '~~',
    after: '~~'
  },
  {
    name: 'link',
    before: '[',
    after: ']()'
  }
]

const previewStyles = ['github', 'air']
const state = {
  preview: '',
  previewStyle: previewStyles[0]
}

const actions = {
  setInput: input => state => ({
    preview: marked(input)
  }),
  changeCss: input => state => ({
    previewStyle: input
  }),
  addEditorItem: itemType => (state, actions) => {
    const editor = document.getElementById('editor')
    const oldInput = editor.value
    const posStart = editor.selectionStart
    const posEnd = editor.selectionEnd

    editor.value =
      oldInput.substring(0, posStart)
      + itemType.before
      + oldInput.substring(posStart, posEnd)
      + itemType.after
      + oldInput.substring(posEnd, oldInput.length)

    editor.focus()
    editor.selectionStart = posStart + itemType.before.length
    editor.selectionEnd =
      posStart
      + itemType.before.length
      + oldInput.substring(posStart, posEnd).length

    actions.setInput(editor.value)
  }
}

import Header from './components/header' // <- Headerコンポーネントを読み込み
import Editor from './components/editor' // <- Editorコンポーネントを読み込み
import Preview from './components/preview' // <- Previewコンポーネントを読み込み

const view = (state, actions) => (
  <div>
    <Header
      previewStyles={previewStyles}
      changeCss={actions.changeCss} /> {/* <- <Header>コンポーネントに置き換え */}

    <article id='main'>
      <Editor editorItems={editorItems} actions={actions} /> {/* <- <Editor>コンポーネントに置き換え */}
      <Preview state={state} /> {/* <-<Preview>コンポーネントに置き換え */}
    </article>
  </div>
)

app(state, actions, view, document.body)